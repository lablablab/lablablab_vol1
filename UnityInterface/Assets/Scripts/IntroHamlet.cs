﻿using UnityEngine;
using System.Collections;

public class IntroHamlet : MonoBehaviour
{
	bool loading = false;
	public RectTransform scroller;
	// Use this for initialization
	void Start ()
	{
		GetComponent<SceneFadeOutCS> ().fade (Color.clear, 1.5f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if((scroller.position.x < -15f) && (!loading)){
			StartCoroutine (fade ());
		}

		if (Input.anyKeyDown && !loading) {
			StartCoroutine (fade ());
		}

	}

	IEnumerator fade ()
	{
		loading = true;
		yield return null;

		GetComponent<SceneFadeOutCS> ().fade (Color.black, 1.5f, "SH_Main");

	}
}

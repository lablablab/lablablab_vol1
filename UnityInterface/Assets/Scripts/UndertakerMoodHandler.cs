﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Mood{
	CBored,
	CDunno,
	CLol,
	CNeutral,
	CSuspicious,
	CThinking,
	CScared
}

public class UndertakerMoodHandler : MonoBehaviour {

	// hack for string enum


	public Sprite Bored;
	public Sprite Dunno;
	public Sprite Lol;
	public Sprite Neutral;
	public Sprite Suspicious;
	public Sprite Thinking;
	public Sprite Scared;



	private Dictionary<Mood, Sprite> moodToSprite;

	void Awake(){
		moodToSprite = new Dictionary<Mood, Sprite>();
		moodToSprite.Add(Mood.CBored, Bored);
		moodToSprite.Add(Mood.CDunno, Dunno);
		moodToSprite.Add(Mood.CLol, Lol);
		moodToSprite.Add(Mood.CNeutral, Neutral);
		moodToSprite.Add(Mood.CSuspicious, Suspicious);
		moodToSprite.Add(Mood.CThinking, Thinking);
		moodToSprite.Add(Mood.CScared, Scared);

	}

	public void ChangeMood(Mood m)
	{
		Debug.Log(m);
		GetComponent<SpriteRenderer>().sprite = moodToSprite[m];
	}
}

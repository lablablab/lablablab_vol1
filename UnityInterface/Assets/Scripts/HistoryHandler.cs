﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System.ComponentModel;

public struct HistoryLine
{

	public HistoryLine (string name, string text)
	{
		this.name = name;
		this.text = text;

	}

	public string name;
	public string text;

}



public enum HistoryName
{
	[Description ("Grave Digger")] Bot,
	[Description ("You")] Player
}


public static class EnumsHelperExtension
{
	public static string ToDescription (this System.Enum en)
	{
		System.Type type = en.GetType ();
		System.Reflection.MemberInfo[] memInfo = type.GetMember (en.ToString ());
		if (memInfo != null && memInfo.Length > 0) {
			object[] attrs = memInfo [0].GetCustomAttributes (typeof(DescriptionAttribute), false);
			if (attrs != null && attrs.Length > 0)
				return ((DescriptionAttribute)attrs [0]).Description;
		}
		return en.ToString ();
	}
}


public class HistoryHandler : MonoBehaviour
{

	public static bool isOpen = false;
	public RectTransform tr;
	float originalHeight;
	public Text history;
	public GameObject playerbubble;

	public RectTransform scrollBarRoot;

	public RectTransform textBoxes;
	public static List<HistoryLine> historyList;
	public ScrollRect scrRect;
	// Use this for initialization
	void Start ()
	{
		originalHeight = tr.sizeDelta.y;
		historyList = new List<HistoryLine> ();
		//feedFakeLines ();
		tr.sizeDelta = new Vector2 (tr.sizeDelta.x, Screen.height * 2.5f / 3);
		tr.anchoredPosition = new Vector2 (tr.anchoredPosition.x, 100);
		history.transform.parent.parent.gameObject.SetActive (false);
	}

	// Update is called once per frame
	public void click ()
	{
		if (isOpen)
			close ();
		else
			open ();
	}


	void open ()
	{
		//tr.anchoredPosition = new Vector2(tr.anchoredPosition.x,500);
		playerbubble.SetActive(false);
		Debug.Log ("in");
		tr.anchoredPosition = new Vector2 (tr.anchoredPosition.x, Screen.height * 2.5f / 3);
		//scrollBarRoot.gameObject.SetActive(true);
		//RectTransform hcTr = tr.Find("historyContainer").GetComponent<RectTransform>();
		//scrollBarRoot.sizeDelta = new Vector2(scrollBarRoot.sizeDelta.x, Screen.height * 2 / 3 - 120);
		//hcTr.sizeDelta = new Vector2(hcTr.sizeDelta.x,Screen.height*2/3 - 120);
		tr.gameObject.GetComponent<InputField> ().enabled = false;
		textBoxes.gameObject.SetActive (false);
		history.gameObject.SetActive (true);
		history.text = "";

		foreach (HistoryLine hl in historyList) {
			history.text += hl.name + " : " + hl.text + "\n";
		}
		isOpen = true;
		history.transform.parent.parent.gameObject.SetActive (true);
		Canvas.ForceUpdateCanvases ();

		scrRect.verticalScrollbar.value = 0f;
		history.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (history.GetComponent<RectTransform> ().anchoredPosition.x, 0);
	}

	void feedFakeLines ()
	{
		string[] fakes = new string[] {
			"hello how are you",
			"who are you",
			"hahahaha i'm not sure i understand",
			"naaah it can't be",
			"well this is quite a conundrum",
			"hum, perhaps",
			"who killed gertrude",
			"i'm sorry i don't know"
		};
		for (int i = 0; i < 50; i++) {
			if (i % 2 == 0) {
				AddLine (HistoryName.Bot, fakes [Random.Range (0, fakes.Length)]);
			} else {
				AddLine (HistoryName.Player, fakes [Random.Range (0, fakes.Length)]);
			}

		}
	}

	public static void AddLine (HistoryName name, string line)
	{
		historyList.Add (new HistoryLine (name.ToDescription (), line));
	}


	void close ()
	{
		playerbubble.SetActive(false);
		// scrollBarRoot.gameObject.SetActive(false);
		//tr.sizeDelta = new Vector2(tr.sizeDelta.x,originalHeight);
		tr.gameObject.GetComponent<InputField> ().enabled = true;
		textBoxes.gameObject.SetActive (true);
		tr.anchoredPosition = new Vector2 (tr.anchoredPosition.x, 100);
		history.transform.parent.parent.gameObject.SetActive (false);
		isOpen = false;

	}


}

using UnityEngine;

public class IntroScript : MonoBehaviour
{


    /*

    This script adds a skip and a fullscreen button, along with sounds when user click them. It uses iTween to fade to the next scene.
    Attach it to an empty that you will use as a GUILayer
    Useful mostly for intros/outros.

    Depend on the iTween plugin for Unity.

    */





    // Dimention and status of the "skip" button


    // Fullscreen button
    Texture fullScreenIcon;
    private bool fullScreenBtn;

    //Array to make a random bank of sound variations.
    public AudioClip[] buttonPressSfx;



    void Start()
    {

        iTween.CameraFadeAdd();
#if UNITY_WEBPLAYER
    testBrowser();
#endif
    }

    void Update()
    {

    
    }

    void OnGUI()
    {

    }

    // Play sounds fx choosen randomly from the array.
    void playSound()
    {
        if (buttonPressSfx.Length >= 1 && buttonPressSfx[0])
        {
            AudioSource.PlayClipAtPoint(buttonPressSfx[Random.Range(0, buttonPressSfx.Length)], transform.position);
        }
    }

    // Load the next scene (in this case, the menu)
    void loadMenu()
    {
        playSound();
        Application.LoadLevel("MainMenu");
    }

    // Fire the isBrowserChrome() js function on the webpage
    void testBrowser()
    {
        Application.ExternalEval("isBrowserChrome()");
    }

    // Is called back by js only if isBrowserChrome() test true
    void browserIsChrome(string param)
    {
        Application.ExternalEval("reloadUnity()");
    }
}

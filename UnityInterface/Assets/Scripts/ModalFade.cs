﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ModalFade : MonoBehaviour
{

    // Use this for initialization

    private static ModalFade _instance;

    Image background;
    public float speed;

		public static bool isOpen = false;

    public static ModalFade i
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ModalFade>();
            }
            return _instance;
        }
    }
    public Color onColor;

    void Start()
    {
        background = GetComponent<Image>();
    }

    public void On()
    {
        StopAllCoroutines();
        StartCoroutine(on());
				isOpen = true;

    }

    public void Off()
    {

			GetComponent<Button>().interactable = false;

        StopAllCoroutines();
        StartCoroutine(off());
        isOpen = false;
    }

     IEnumerator on()
    {
				background.enabled = true;
        float i = 0;
        while (i < 1)
        {
            background.color = Color.Lerp(new Color(0,0,0,0), onColor, i);
            i += speed;
            yield return null;
        }
				background.color = Color.Lerp(new Color(0,0,0,0), onColor, 1);
				GetComponent<Button>().interactable = true;
    }

		public void CloseModal(){
				OverlayToggle.i.Off();
				ModalFade.i.Off();

		}

     IEnumerator off()
    {
        yield return new WaitForSeconds(.3f);
        float i = 1;
        while (i > 0)
        {
            background.color = Color.Lerp(new Color(0,0,0,0), onColor, i);
            i -= speed;
            yield return null;
        }
				background.color = Color.Lerp(new Color(0,0,0,0), onColor, 0);
				background.enabled = false;
    }


}

using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class ChatScript_Com : MonoBehaviour
{



	public Text botText;
	public GameObject botBubble;
	public InputField playerInputWindow;


	// private string ChatScripUrl = "https://127.0.0.1/sh_client.php?";
	// private string NewIDUrl = "https://127.0.0.1/sh_id.php?";
	private string ChatScripUrl = "https://lablablab.net/lablablab/sh_client.php?";
	private string NewIDUrl = "https://lablablab.net/lablablab/sh_id.php?";
	private string userID = "globule";
	private int userNumber = 0;
	private string consoleText = "";
	private int session = 0;
	private bool endgame = false;

	public float textDelay = 0.2f;

	public UndertakerMoodHandler mood;
	public GameMemory gmem;
	public HeadStoneProgress ClaudiusHeadstone;
	public HeadStoneProgress GertrudeHeadstone;
	public HeadStoneProgress PoloniusHeadstone;
	public HeadStoneProgress HamletSrHeadstone;
	public HeadStoneProgress HamletJrHeadstone;
	public HeadStoneProgress OpheliaHeadstone;
	public HeadStoneProgress LaertesHeadstone;
	public HeadStoneProgress RozencrantzHeadstone;
	public HeadStoneProgress GuildensternHeadstone;
    HeadStoneProgress[] hsArray;



    private Lookup<string, HeadStoneProgress> stringToHeadstone;
	public GameObject playerBubble;
	public Text playerText;

	public InputField PlayerInput;

	public GameObject dunnoMainObject;
	public GameObject instantiatedDunno;

	public RectTransform dunnoAnchor;

	public Transform Fireworks;

	IEnumerator Start ()
	{

		gmem = GameMemory.GetInstance();
		gmem.Load();
		userNumber =gmem.sh_id;
		session = gmem.sh_session;
		setTombStates();

		hsArray = new HeadStoneProgress[] {
			ClaudiusHeadstone,
			GertrudeHeadstone,
			PoloniusHeadstone,
			HamletSrHeadstone,
			HamletJrHeadstone,
			OpheliaHeadstone,
			LaertesHeadstone,
			RozencrantzHeadstone,
			GuildensternHeadstone
		};

		stringToHeadstone = (Lookup<string,HeadStoneProgress>)hsArray.ToLookup (p => p.identifier, p => p);

		foreach (var item in stringToHeadstone) {
			Debug.Log (item.Key);
		}

		playerBubble.SetActive (false);
		botBubble.SetActive (false);


		if(gmem.sh_id==42){
			yield return StartCoroutine (getNewID ());
		}

		Camera.main.GetComponent<SceneFadeOutCS> ().StopAllCoroutines ();
		yield return StartCoroutine (Camera.main.GetComponent<SceneFadeOutCS> ().fadeTo (Color.clear, 1.5f));


		initConversation ();



		playerText = playerBubble.GetComponentInChildren<Text> ();



		//  StartCoroutine(Says(botText, "CDunno"));
		yield return null;

        //yield return StartCoroutine (test ());

	}


    public bool IsGameComplete() {
        foreach (var item in hsArray)
        {



            if (!item.Completed) {
                return false;
            }
        }

        return true;

    }

	void setTombStates(){
		//claudius
		if(gmem.claudius[0])
			ClaudiusHeadstone.activate(Milestone.When);
		if(gmem.claudius[1])
			ClaudiusHeadstone.activate(Milestone.How);		
		if(gmem.claudius[2])
			ClaudiusHeadstone.activate(Milestone.Where);
		if(gmem.claudius[3])
			ClaudiusHeadstone.activate(Milestone.Who);
		if(gmem.claudius[4])
			ClaudiusHeadstone.activate(Milestone.Why);	
		//laertes
		if(gmem.laertes[0])
			LaertesHeadstone.activate(Milestone.When);
		if(gmem.laertes[1])
			LaertesHeadstone.activate(Milestone.How);		
		if(gmem.laertes[2])
			LaertesHeadstone.activate(Milestone.Where);
		if(gmem.laertes[3])
			LaertesHeadstone.activate(Milestone.Who);
		if(gmem.laertes[4])
			LaertesHeadstone.activate(Milestone.Why);	
		//ophelia
		if(gmem.ophelia[0])
			OpheliaHeadstone.activate(Milestone.When);
		if(gmem.ophelia[1])
			OpheliaHeadstone.activate(Milestone.How);		
		if(gmem.ophelia[2])
			OpheliaHeadstone.activate(Milestone.Where);
		if(gmem.ophelia[3])
			OpheliaHeadstone.activate(Milestone.Who);
		if(gmem.ophelia[4])
			OpheliaHeadstone.activate(Milestone.Why);		
		//polonius
		if(gmem.polonius[0])
			PoloniusHeadstone.activate(Milestone.When);
		if(gmem.polonius[1])
			PoloniusHeadstone.activate(Milestone.How);		
		if(gmem.polonius[2])
			PoloniusHeadstone.activate(Milestone.Where);
		if(gmem.polonius[3])
			PoloniusHeadstone.activate(Milestone.Who);
		if(gmem.polonius[4])
			PoloniusHeadstone.activate(Milestone.Why);	
		//hamletsr
		if(gmem.hamletsr[0])
			HamletSrHeadstone.activate(Milestone.When);
		if(gmem.hamletsr[1])
			HamletSrHeadstone.activate(Milestone.How);		
		if(gmem.hamletsr[2])
			HamletSrHeadstone.activate(Milestone.Where);
		if(gmem.hamletsr[3])
			HamletSrHeadstone.activate(Milestone.Who);
		if(gmem.hamletsr[4])
			HamletSrHeadstone.activate(Milestone.Why);	
		//hamletjr
		if(gmem.hamletjr[0])
			HamletJrHeadstone.activate(Milestone.When);
		if(gmem.hamletjr[1])
			HamletJrHeadstone.activate(Milestone.How);		
		if(gmem.hamletjr[2])
			HamletJrHeadstone.activate(Milestone.Where);
		if(gmem.hamletjr[3])
			HamletJrHeadstone.activate(Milestone.Who);
		if(gmem.hamletjr[4])
			HamletJrHeadstone.activate(Milestone.Why);			
		//rozen
		if(gmem.rozen[0])
			RozencrantzHeadstone.activate(Milestone.When);
		if(gmem.rozen[1])
			RozencrantzHeadstone.activate(Milestone.How);		
		if(gmem.rozen[2])
			RozencrantzHeadstone.activate(Milestone.Where);
		if(gmem.rozen[3])
			RozencrantzHeadstone.activate(Milestone.Who);
		if(gmem.rozen[4])
			RozencrantzHeadstone.activate(Milestone.Why);	
		//guilden
		if(gmem.guilden[0])
			GuildensternHeadstone.activate(Milestone.When);
		if(gmem.guilden[1])
			GuildensternHeadstone.activate(Milestone.How);		
		if(gmem.guilden[2])
			GuildensternHeadstone.activate(Milestone.Where);
		if(gmem.guilden[3])
			GuildensternHeadstone.activate(Milestone.Who);
		if(gmem.guilden[4])
			GuildensternHeadstone.activate(Milestone.Why);			
		//gertrude
		if(gmem.gertrude[0])
			GertrudeHeadstone.activate(Milestone.When);
		if(gmem.gertrude[1])
			GertrudeHeadstone.activate(Milestone.How);		
		if(gmem.gertrude[2])
			GertrudeHeadstone.activate(Milestone.Where);
		if(gmem.gertrude[3])
			GertrudeHeadstone.activate(Milestone.Who);
		if(gmem.gertrude[4])
			GertrudeHeadstone.activate(Milestone.Why);		
	}

	void saveTombStates(){
		gmem.claudius[0]=ClaudiusHeadstone.getState(0);
		gmem.claudius[1]=ClaudiusHeadstone.getState(1);
		gmem.claudius[2]=ClaudiusHeadstone.getState(2);
		gmem.claudius[3]=ClaudiusHeadstone.getState(3);
		gmem.claudius[4]=ClaudiusHeadstone.getState(4);
		gmem.ophelia[0]=OpheliaHeadstone.getState(0);
		gmem.ophelia[1]=OpheliaHeadstone.getState(1);
		gmem.ophelia[2]=OpheliaHeadstone.getState(2);
		gmem.ophelia[3]=OpheliaHeadstone.getState(3);
		gmem.ophelia[4]=OpheliaHeadstone.getState(4);
		gmem.polonius[0]=PoloniusHeadstone.getState(0);
		gmem.polonius[1]=PoloniusHeadstone.getState(1);
		gmem.polonius[2]=PoloniusHeadstone.getState(2);
		gmem.polonius[3]=PoloniusHeadstone.getState(3);
		gmem.polonius[4]=PoloniusHeadstone.getState(4);
		gmem.laertes[0]=LaertesHeadstone.getState(0);
		gmem.laertes[1]=LaertesHeadstone.getState(1);
		gmem.laertes[2]=LaertesHeadstone.getState(2);
		gmem.laertes[3]=LaertesHeadstone.getState(3);
		gmem.laertes[4]=LaertesHeadstone.getState(4);
		gmem.hamletsr[0]=HamletSrHeadstone.getState(0);
		gmem.hamletsr[1]=HamletSrHeadstone.getState(1);
		gmem.hamletsr[2]=HamletSrHeadstone.getState(2);
		gmem.hamletsr[3]=HamletSrHeadstone.getState(3);
		gmem.hamletsr[4]=HamletSrHeadstone.getState(4);
		gmem.hamletjr[0]=HamletJrHeadstone.getState(0);
		gmem.hamletjr[1]=HamletJrHeadstone.getState(1);
		gmem.hamletjr[2]=HamletJrHeadstone.getState(2);
		gmem.hamletjr[3]=HamletJrHeadstone.getState(3);
		gmem.hamletjr[4]=HamletJrHeadstone.getState(4);
		gmem.guilden[0]=GuildensternHeadstone.getState(0);
		gmem.guilden[1]=GuildensternHeadstone.getState(1);
		gmem.guilden[2]=GuildensternHeadstone.getState(2);
		gmem.guilden[3]=GuildensternHeadstone.getState(3);
		gmem.guilden[4]=GuildensternHeadstone.getState(4);
		gmem.rozen[0]=RozencrantzHeadstone.getState(0);
		gmem.rozen[1]=RozencrantzHeadstone.getState(1);
		gmem.rozen[2]=RozencrantzHeadstone.getState(2);
		gmem.rozen[3]=RozencrantzHeadstone.getState(3);
		gmem.rozen[4]=RozencrantzHeadstone.getState(4);
		gmem.gertrude[0]=GertrudeHeadstone.getState(0);
		gmem.gertrude[1]=GertrudeHeadstone.getState(1);
		gmem.gertrude[2]=GertrudeHeadstone.getState(2);
		gmem.gertrude[3]=GertrudeHeadstone.getState(3);
		gmem.gertrude[4]=GertrudeHeadstone.getState(4);

		gmem.Save();


	}

	IEnumerator test ()
	{
		yield return null;

		string[] characters = new string[] {
			"CClaudius",
			"CGertrude",
			"CPolonius",
			"CHamletSr",
			"CHamletJr",
			"COphelia",
			"CLaertes",
			"CGR"
		};

		string[] milestone = new string[] {
			"When",
			"Who",
			"Where",
			"Why",
			"How"
		};

		foreach (var item in characters) {
			foreach (var item2 in milestone) {
				parseCodes (item + item2);

			}
		}


	}


	void OnGUI ()
	{
		if (Event.current.keyCode == KeyCode.Return && Event.current.type == EventType.KeyDown && PlayerInput.text != "") {
			Debug.Log (EventSystem.current.currentSelectedGameObject);
			if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject == PlayerInput.gameObject) {
				sendText (PlayerInput.text);
				PlayerInput.text = "";
			}
		}
	}

	IEnumerator botSays (string bubbletext)
	{



		yield return StartCoroutine (Says (botText, bubbletext));
		//Set focus on input window
		playerInputWindow.interactable = true;
		playerInputWindow.ActivateInputField ();
	}

	public void SetSize (RectTransform trans, Vector2 newSize)
	{
		Vector2 oldSize = trans.rect.size;
		Vector2 deltaSize = newSize - oldSize;
		trans.offsetMin = trans.offsetMin - new Vector2 (deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
		trans.offsetMax = trans.offsetMax + new Vector2 (deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
	}

	void resizeBubble (Text bubble, string bubbletext)
	{
		RectTransform rt = bubble.rectTransform.parent.GetComponent<RectTransform> ();

		rt.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
		bubble.text = bubbletext;
		Canvas.ForceUpdateCanvases ();

		for (int i = 0; i < 10; i++) {
			float width = rt.rect.width;
			float height = rt.rect.height;
			float ratio = width / height;
			//Debug.Log (width + "  " + height);
			//Debug.Log ("before ratio " + ratio);

			SetSize (rt, new Vector2 (height * 4f, height));
			//rt.sizeDelta = new Vector2(200 ,200);

			//Debug.Log (ratio);
			Canvas.ForceUpdateCanvases ();
		}
		rt.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.Unconstrained;
		bubble.text = "";
	}



	IEnumerator Says (Text who, string what)
	{
        
		what = cleanString (what);
		what = parseCodes (what);
		who.text = "";

		if (who == botText) {
			if (instantiatedDunno) {
				Destroy (instantiatedDunno);
			}


			HistoryHandler.AddLine (HistoryName.Bot, what);
			if (what.Length == 0) {
				botBubble.SetActive (false);
				instantiatedDunno = Instantiate (dunnoMainObject) as GameObject;

				var itrans = instantiatedDunno.GetComponent<RectTransform> ();

				itrans.SetParent (this.transform, false);
				itrans.pivot = dunnoAnchor.pivot;
				itrans.anchorMin = dunnoAnchor.anchorMin;
				itrans.anchorMax = dunnoAnchor.anchorMax;
				itrans.sizeDelta = dunnoAnchor.sizeDelta;
				itrans.position = dunnoAnchor.position;
				instantiatedDunno.GetComponent<DunnoScript> ().maxMove = new Vector2 (dunnoAnchor.rect.width, dunnoAnchor.rect.height);

				instantiatedDunno.GetComponent<DunnoScript> ().launch ();

				//hide bubble and show ???
			} else {
				//hide ??? and show bubble
				botBubble.SetActive (true);
			}
		} else {
			HistoryHandler.AddLine (HistoryName.Player, what);
		}

		resizeBubble (who, what);



		foreach (var letter in what.ToCharArray()) {
			who.text += letter;
			yield return new WaitForSeconds (textDelay * Random.Range (0.35f, 0.55f)); // Original Random.Range(0.5, 2)
		}
		/*if (who == botText) {
			playerBubble.SetActive (false);
		}*/
		if(endgame&&(who==botText)){
			StartCoroutine(botSays("Hey! You did it! You solved Hamlet!"));
			Instantiate(Fireworks,new Vector3(0.3f,-17.0f,-60.0f),transform.rotation);
			endgame = false;
		}

	}

	IEnumerator playerSays (string bubbletext)
	{

		playerBubble.SetActive (true);
		StartCoroutine (Says (playerText, char.ToUpper (bubbletext [0]) + bubbletext.Substring (1)));
		yield return null;
	}

	string parseCodes (string parseText)
	{
		//mood
		string[] emo = new string[] { "CBored", "CDunno", "CLol", "CNeutral", "CSuspicious", "CThinking", "CScared" };
		bool moodFound = false;
		foreach (string s in emo) {
			if (parseText.Contains (s)) {
				moodFound = true;
				var newMood = (Mood)System.Enum.Parse (typeof(Mood), s);
				mood.ChangeMood (newMood);
				if (newMood == Mood.CDunno) {
					Debug.Log ("detecteddunno");
					parseText = "";
					return parseText;
				} else {
					parseText = parseText.Remove (parseText.IndexOf (s), s.Length);
				}

			}
		}

		if (!moodFound) {
			mood.ChangeMood (Mood.CNeutral);
		}

		string[] characters = new string[] {
			"CClaudius",
			"CGertrude",
			"CPolonius",
			"CHamletSr",
			"CHamletJr",
			"COphelia",
			"CLaertes",
			"CGR"
		};
		//string[] ms = new string[] { "When","How","Where","Whom","Why"};
		foreach (string cr in characters) {


			if (parseText.Contains (cr)) {

				var startMilestone = parseText.IndexOf (cr) + cr.Length;
				var endMilestone = parseText.IndexOfAny (" ,\n".ToCharArray (), startMilestone);
				endMilestone = endMilestone < 0 ? parseText.Length : endMilestone;
				string milestone = parseText.Substring (startMilestone, endMilestone - startMilestone);


				HeadStoneProgress[] hsp = stringToHeadstone [cr].ToArray ();
				Debug.Log (hsp.Length);
				foreach (var item in hsp) {
					item.activate ((Milestone)System.Enum.Parse (typeof(Milestone), milestone));

				}

				parseText = parseText.Remove (parseText.IndexOf (cr), endMilestone - parseText.IndexOf (cr));

				if(IsGameComplete()){
					endgame = true;
				}
			}
		}
		saveTombStates();
		parseText = System.Text.RegularExpressions.Regex.Replace (parseText, @"\s+", " ");
		return parseText;
	}

	public void sendText (string userInput)
	{

		playerInputWindow.interactable = false;
		playerInputWindow.DeactivateInputField ();

		userInput = cleanString (userInput);
		playSound ();
		if(userInput==""){
			userInput="oï";
		}
		consoleText = consoleText + "\n\n[Player]: " + userInput;
		StartCoroutine (playerSays (userInput));
		StartCoroutine (postMessage (userInput, false, false));
	}

	string cleanString (string s)
	{
		s = s.Replace ("\r", "");
		s = s.Replace ("\n", "");
		return s;
	}

	public void restart(){
		session++;
		gmem.sh_session = session;
		ClaudiusHeadstone.deactivateAll();
		GertrudeHeadstone.deactivateAll();
		PoloniusHeadstone.deactivateAll();
		HamletSrHeadstone.deactivateAll();
		HamletJrHeadstone.deactivateAll();
		OpheliaHeadstone.deactivateAll();
		LaertesHeadstone.deactivateAll();
		RozencrantzHeadstone.deactivateAll();
		GuildensternHeadstone.deactivateAll();
		saveTombStates();
		gmem.Save();
		initConversation();
	}

	void initConversation ()
	{
		
		userID = "id_" + userNumber + "_session_" + session;
		Debug.Log("user id: "+userID);
		StartCoroutine (postMessage ("Oï", false, false));
	}

	IEnumerator getNewID ()
	{
		WWW w = new WWW (NewIDUrl);

		yield return w;
		if (!string.IsNullOrEmpty (w.error)) {
			StartCoroutine (botSays ("I have a problem communicating with my brain, I need to have access to the internet."));
		} else {
			Debug.Log (w.text);
			userNumber = int.Parse (w.text); // Retrieve bot response for display in text bubble
		}
		gmem.sh_id = userNumber;
		gmem.Save();
	}

	IEnumerator postMessage (string message, bool pause, bool silent)
	{


		Debug.Log ("sending message");
		message = message.Replace (":", ""); // avoid users sending commands to server
		if (message == "") {
			message = "bob";
		}

		var msgURL = ChatScripUrl + "message=" + WWW.EscapeURL (message) + "&userID=" + WWW.EscapeURL (userID);
		Debug.Log (msgURL);
		WWW w = new WWW (msgURL);

		yield return w;

		if (!string.IsNullOrEmpty (w.error)) {
			StartCoroutine (botSays ("I have a problem communicating with my brain, please try talking to me later."));
		} else {
			if (!silent) {
				StartCoroutine (botSays (w.text.Trim ()));
			}
		}
	}

	// Button sounds
	void playSound ()
	{

	}
}

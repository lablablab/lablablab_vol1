using UnityEngine;
using System.Collections;

public class SceneFadeOutCS : MonoBehaviour
{



	void Awake ()
	{
		// Set the texture so that it is the the size of the screen and covers it.


		GetComponent<GUITexture> ().pixelInset = new Rect (0f, 0f, Screen.width, Screen.height);
		GetComponent<GUITexture> ().enabled = true;

	}

	public void fade (Color c, float fadeTime, string scenetoload = "")
	{
		StopAllCoroutines ();
		StartCoroutine (fadeTo (c, fadeTime, scenetoload));
	}

	public IEnumerator fadeTo (Color c, float fadeTime, string scenetoload = "")
	{
		Color startColor = GetComponent<GUITexture> ().color;
		float index = 0;
		while (index < 1) {
			GetComponent<GUITexture> ().color = Color.Lerp (startColor, c, index);
			index += Time.deltaTime / fadeTime;
			yield return null;
		}
		if (scenetoload != "") {
			Application.LoadLevel (scenetoload);
		}
	}



}

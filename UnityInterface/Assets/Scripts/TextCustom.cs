﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	/// <summary>
	/// Labels are graphics that display text.
	/// </summary>

	public class TextCustom : Text
	{

		public void onCopy ()
		{
			GUIUtility.systemCopyBuffer = this.text;
		}

	}

}
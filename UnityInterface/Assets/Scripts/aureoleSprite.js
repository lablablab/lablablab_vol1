private var aureolesObj : Component[]; //Array of objects
private	var aurY : float = 1f; // Y max value for the cos wobble
private	var aurAmp : float = 0.002f; // Amplitude for the cos wobble
private var aurIndex : float; // Index for the cos wobble
private var aurA : float; // Alpha target for the fadein
private var aurCA : float; // Current alpha for the fade in


function Awake() {
fillArray(); 
}

function Start(){
}

function Update() {
	aurMovement();
	aurAlpha();	
}

private function aurMovement() { // Update the movement to create a slight wobble
	for (var obj : Component in aureolesObj) {
			aurIndex += Time.deltaTime; 
			var y = aurAmp*Mathf.Cos(aurY*aurIndex)+Random.Range(0.004f,-0.004f);
			obj.GetComponent.<Renderer>().transform.position = new Vector3(obj.GetComponent.<Renderer>().transform.position.x,obj.GetComponent.<Renderer>().transform.position.y+y,0); // Make sure that we slide from current y to target y and back
	}
}

private function fillArray() { // Populate the array
aureolesObj = gameObject.GetComponentsInChildren(typeof(SpriteRenderer)); // Fetch the gameobjects's children of type "SpriteRenderer"
	for (var obj : Component in aureolesObj) { 
			obj.GetComponent.<Renderer>().material.color.a = 0; // Make sure that our objects alphas are 0
		}
}

private function aurAlpha() { // Update our auréoles's alpha
	for (var obj : Component in aureolesObj) {
		aurCA = obj.GetComponent.<Renderer>().material.color.a; // Register current alpha
		obj.GetComponent.<Renderer>().material.color.a += (aurA-aurCA)*0.1f; //Log toward target alpha
	}
}

public function aureoleAlphaSet(alpha:float, maxAlpha:float) { // Set the target alpha (use the same value as ProgressBar)
	aurA = 1f*alpha/maxAlpha; // Map value over 1.0 (max alpha float)
}
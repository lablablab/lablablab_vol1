﻿

#pragma strict
import UnityEngine;
import UnityEngine.UI;
import UnityEngine.EventSystems;
import UnityEngine.Events;
import System.Collections;

import System;
import System.Runtime.Serialization.Formatters.Binary;
import System.IO;



//make a private variable to store instance of GameMemory that can only be accessed by the GameMemory class.
private static var gmem : GameMemory;
//make a public function that stores the instance of GameMemory in the private variable and returns the variable.
public static function GetInstance() : GameMemory{
	if (!gmem) {
           gmem = FindObjectOfType(typeof (GameMemory)) as GameMemory;
           
           //DontDestroyOnLoad(gmem.gameObject);
            if (!gmem)
                Debug.LogError ("There needs to be one active GameMemory script on a GameObject in your scene.");
	}
	
	return gmem;
}

public var SAVE = true;

public var introTut1 = true;
public var introTut2 = false;
public var introTut3 = false;
public var introTut4 = false;
public var walkthrough = false;
public var helpTut = false;
public var ranks:Array = new Array();


function Awake(){
	/*if(gmem == null){
		DontDestroyOnLoad(this.gameObject);
		gmem = this;
		
		
	}else if(gmem != this){
		Destroy (gameObject);
	}*/

	Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER","yes");	


	setQuestVariable();
	
}
function Start(){
	//Load();
	//définir ranks
	ranks.Push("Rogue Beginner");
	ranks.Push("Rogue Beginner");
	ranks.Push("Rock Jumper");
	ranks.Push("Bubble Popper");
	ranks.Push("Bomb Detonator");
	ranks.Push("Portal Passer");
	ranks.Push("Marble Pusher");
	ranks.Push("Board Explorer");
	ranks.Push("Stairway Finder");
	ranks.Push("Sequence Maker");
	ranks.Push("Marble Devourer");
	ranks.Push("Rogue Hopper");
	ranks.Push("Solitaire Cruncher");
	ranks.Push("Solitaire Master");
	ranks.Push("Solitaire Hero");



}


////////////////////////////////////////////////
//////////////       MEDALS     ///////////////
//medal variables
public var firsMedal = false;

public var goldCount : int = 0;
public var silverCount : int = 0;
public var copperCount : int = 0;
//adventure variables
public var goldSeriesCount : int = 0;
public var silverSeriesCount : int = 0;
public var copperSeriesCount : int = 0;

public var totalMedalsCount : int = 0;

public var numRetries:int = 0;
public var numLevel:int = 1;
public var maxLevel:int = 0;
public var plateauNum:int = 0;
public var numBoules:int = 0;
public var retrycost:int = 1;
public var unlockLevel:int = 1;

public var ts_id:int = 42;
public var ts_session:int = 1;
public var ts_trust:int = 0;
public var ts_patience:int = 6;
public var sp_id:int = 42;
public var sp_session:int = 1;
public var sp_ambar:int = 0;
public var sp_mouton:int = 0;
public var sp_curGambits:int = 0;
public var sh_id:int = 42;
public var sh_session:int = 1;
public var claudius = [false,false,false,false,false] ;
public var laertes = [false,false,false,false,false] ;
public var ophelia = [false,false,false,false,false] ;
public var polonius = [false,false,false,false,false] ;
public var hamletsr = [false,false,false,false,false] ;
public var hamletjr = [false,false,false,false,false] ;
public var rozen = [false,false,false,false,false] ;
public var guilden = [false,false,false,false,false] ;
public var gertrude = [false,false,false,false,false] ;

public var niveauxvus:Array = new Array();

//medal functions
function AddGold(){
	goldCount++;
	totalMedalsCount++;
}
function AddSilver(){
	silverCount++;
	totalMedalsCount++;
}
function AddCopper(){
	copperCount++;
	totalMedalsCount++;
	//mq.CheckForQuests();
}


//adventure functions
function AddGoldSeries(){
	goldSeriesCount++;
	//totalMedalsCount++;
}
function AddSilverSeries(){
	silverSeriesCount++;
	//totalMedalsCount++;
}
function AddCopperSeries(){
	copperSeriesCount++;
	//totalMedalsCount++;
	//mq.CheckForQuests();
}
////////////////////////////////////////////////
//////////////       QUESTS     //////////////
var tqn : int;
var quests : boolean[];
//var lastQuestChecked :int = 0;
function setQuestVariable(){

//tqn = 62;
quests  = new boolean[tqn];
}
//Debug.Log(quests[1]);
public function Save(){
//Debug.Log("SAVING");
	 var bf = new BinaryFormatter();
	 var file:FileStream = File.Create(Application.persistentDataPath + "/playerInfo.dat");	
	 
	 var data: PlayerData = new PlayerData();
	data.numRetries = numRetries;
	data.numLevel = numLevel;
	data.maxLevel = maxLevel;
	data.plateauNum = plateauNum;
	data.numBoules = numBoules;
	data.retrycost = retrycost;
	data.unlockLevel = unlockLevel;
	data.ts_id = ts_id;
	data.ts_session = ts_session;
	data.ts_trust = ts_trust;
	data.ts_patience = ts_patience;
	data.sp_id = sp_id;
	data.sp_session = sp_session;
	data.sp_ambar = sp_ambar;
	data.sp_mouton = sp_mouton;
	data.sp_curGambits = sp_curGambits;
	data.sh_id = sh_id;
	data.sh_session = sh_session;
	data.claudius = claudius;
	data.laertes = laertes;
	data.ophelia = ophelia;
	data.polonius = polonius ;
	data.hamletsr = hamletsr;
	data.hamletjr = hamletjr ;
	data.rozen = rozen ;
	data.guilden = guilden ;
	data.gertrude= gertrude;
	data.niveauxvus = niveauxvus;
 	 
	 data.introTut1 = introTut1;
	 data.introTut2 = introTut2;
	 data.introTut3 = introTut3;
	 data.introTut4 = introTut4;
	 data.walkthrough = walkthrough;
	 data.helpTut = helpTut;
	 data.walkthrough = walkthrough;
	 
 	data.firsMedal = firsMedal;

	data.goldCount= goldCount;
	data.silverCount = silverCount;
	data.copperCount = copperCount;
	//adventure variables
	data.goldSeriesCount = goldSeriesCount;
	data.silverSeriesCount = silverSeriesCount;
	data.copperSeriesCount = copperSeriesCount;

	data.totalMedalsCount = totalMedalsCount;
	data.quests = quests; 
	 
	 bf.Serialize(file, data);
	 file.Close();
}
public function Load(){
	//Debug.Log("LOADING FROM playerInfo.dat");
	if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")){
	//Debug.Log("playerInfo.dat LOADED");	
		var bf = new BinaryFormatter();
		var file:FileStream = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
		
		var data: PlayerData = bf.Deserialize(file);
		file.Close();

		numRetries = data.numRetries;
		numLevel = data.numLevel;
		maxLevel = data.maxLevel ;
		plateauNum = data.plateauNum;
		numBoules = data.numBoules;
		retrycost = data.retrycost;
		unlockLevel = data.unlockLevel;
		sp_id = data.sp_id;
		sp_session = data.sp_session;
		sp_ambar = data.sp_ambar;
		sp_mouton = data.sp_mouton;
		sp_curGambits = data.sp_curGambits;
		sh_id = data.sh_id;
		sh_session = data.sh_session;
		claudius = data.claudius;
		laertes = data.laertes;
		ophelia = data.ophelia;
		polonius = data.polonius ;
		hamletsr = data.hamletsr;
		hamletjr = data.hamletjr ;
		rozen = data.rozen ;
		guilden = data.guilden ;
		gertrude = data.gertrude;
		ts_id = data.ts_id;
		ts_session = data.ts_session;
		ts_trust = data.ts_trust;
		ts_patience = data.ts_patience;
		niveauxvus = data.niveauxvus;
						
		introTut1 = data.introTut1;
		introTut2 = data.introTut2;
		introTut3 = data.introTut3;
		introTut4 = data.introTut4;
		walkthrough = data.walkthrough;
	 	helpTut = data.helpTut;
	 	
	 	
	 	
	 	firsMedal = data.firsMedal;

		goldCount= data.goldCount;
		silverCount = data.silverCount;
		copperCount = data.copperCount;
		//adventure variables
		goldSeriesCount = data.goldSeriesCount;
		silverSeriesCount = data.silverSeriesCount;
		copperSeriesCount = data.copperSeriesCount;

		totalMedalsCount = data.totalMedalsCount;
		
		quests = data.quests; 
	 }else {
	 Debug.Log("NOTHING TO LOAD");
	 }
}		

public function getRank(number:int){

	return ranks[number];

}
			
public function ResetApp(){
Debug.Log(" RESETTING APP");
	if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")){
	Debug.Log(" APP RESET");
		File.Delete(Application.persistentDataPath + "/playerInfo.dat");
	}else{
	Debug.Log("NOTHING TO RESET");
	}
}
	


//[Serializable]
class PlayerData extends System.Object{

	public var introTut1 = true;
	public var introTut2 = false;
	public var introTut3 = false;
	public var introTut4 = false;
	public var walkthrough = false;
	public var helpTut = false;
	public var firsMedal = false;

	public var goldCount : int = 0;
	public var silverCount : int = 0;
	public var copperCount : int = 0;
	//adventure variables
	public var goldSeriesCount : int = 0;
	public var silverSeriesCount : int = 0;
	public var copperSeriesCount : int = 0;

	public var totalMedalsCount : int = 0;
	public var quests : boolean[];
	public var numRetries:int = 0;
	public var numLevel:int = 1;
	public var maxLevel:int = 0;	
	public var plateauNum:int = 0;
	public var numBoules:int = 0;
	public var retrycost:int = 1;
	public var unlockLevel:int = 1;
    public var ts_id:int = 42;
    public var ts_session:int = 1;
    public var ts_trust:int = 0;
    public var ts_patience:int = 6;
    public var sp_id:int = 42;
    public var sp_session:int = 1;
    public var sp_ambar:int = 0;
    public var sp_mouton:int = 0;
    public var sp_curGambits:int = 0;
    public var sh_id:int = 42;
    public var sh_session:int = 1;
    public var claudius = [false,false,false,false,false] ;
	public var laertes = [false,false,false,false,false] ;
	public var ophelia = [false,false,false,false,false] ;
	public var polonius = [false,false,false,false,false] ;
	public var hamletsr = [false,false,false,false,false] ;
	public var hamletjr = [false,false,false,false,false] ;
	public var rozen = [false,false,false,false,false] ;
	public var guilden = [false,false,false,false,false] ;
	public var gertrude = [false,false,false,false,false] ;

	public var niveauxvus:Array = new Array();

//	quests  = new boolean[tqn];
}

function OnApplicationQuit() {
		// Save
		if(SAVE){
			Save();
		}else{
			ResetApp();
		}
	}